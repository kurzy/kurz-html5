import Input from './Input';

class Checkbox extends Input {
  public constructor(name: string, checked = false) {
    super('checkbox', null, name);
    this.element.checked = checked;
    this.element.setAttribute('role', 'switch');
  }

  get checked(): boolean {
    return this.element.checked;
  }
}

export default Checkbox;
