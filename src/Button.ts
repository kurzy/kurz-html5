class Button {
  private button: HTMLButtonElement;

  public constructor(textContent: string) {
    const button = document.createElement('button')
    button.type = 'button';
    this.button = button;
    this.bleble = textContent;
  }

  public addToParent(parent: Element): void {
    parent.appendChild(this.button);
  }

  set onClick(onClickHandler: () => void) {
    this.button.addEventListener('click', onClickHandler);
  }

  set bleble(text: string) {
    if (!text) {
      throw new Error('Can\'t set empty text');
    }

    this.button.textContent = text;
  }
}

export default Button;
