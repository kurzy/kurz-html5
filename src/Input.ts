import { TInputValue, TInputType } from './types';
class Input {
  protected element: HTMLInputElement;
  public constructor(
    type: TInputType,
    value: TInputValue,
    name: string,
  ) {
    const element = document.createElement('input');
    element.type = type;
    element.value = value?.toString();
    element.name = name;
    element.id = name;

    this.element = element;
  }

  get value(): string {
    return this.element.value;
  }

  public addToParent(parent: Element): void {
    parent.appendChild(this.element);
  }
}

export default Input;
