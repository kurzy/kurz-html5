import { getSentence } from './utils';
import Input from './Input';
import Checkbox from './Checkbox';
import Button from './Button';
import { IColor } from './types';

console.log('Hello modules!');
console.log(getSentence());

const firtsInput = new Input('password', 'moje tajné heslo', 'password');

const app = document.querySelector('#app');
firtsInput.addToParent(app);

const checkbox = new Checkbox('checkbox');
checkbox.addToParent(app);


const button = new Button('Klikni');
button.addToParent(app);
// button.bleble = 'bleble';
button.onClick = () => {
  console.log('Kliknuto');
  console.log(checkbox.checked, firtsInput.value);
};

const redColor: IColor = {
  red: 255, green: 0, blue: 0, name: 'Red',
};
console.log(redColor);
const anyColor: IColor = {
  red: 128, green: 42, blue: 18,
}


const genialNumbers = {
  theAnswer: 42,
  pi: 3.1452,
  goldenRatio: 1.618,
  euler: 2.718,
  baker: 221,
};

type TGenialNumberKey = keyof typeof genialNumbers;

const getGenialNumber = (key: TGenialNumberKey) => genialNumbers[key];

console.log(getGenialNumber('pi'));
// console.log(getGenialNumber('idn'));


const rectangleArea = (a: number, b: number): string => {
  const area = a * b;
  return area.toFixed(2);
}

type TInitiaData = {
  default: string;
  sum: (a: number, b: number, ...rest: Array<number>) => number;
  initialData?: {
    a: string;
    b: number;
  }
};

const redukuj = (subtotal: number, entry: number) => subtotal + entry;

const init: TInitiaData = {
  default: 'Karel',
  sum: (x, y, ...zbytek) => {
    const summary = x + y;
    const total = zbytek.reduce(redukuj, summary);
    return total;
  },
};

// console.log(init.sum(5));
console.log(init.sum(5, 2));
console.log(init.sum(5, 2, 4, 3, 1));
console.log(init.initialData?.a);


const isNumberArray = (value: unknown): value is Array<number> => {
  return Array.isArray(value) && value.every((entry) => typeof entry === 'number');
};

console.log(isNumberArray(45));
console.log(isNumberArray('Jolanda'));
console.log(isNumberArray(['Jolanda']));
console.log(isNumberArray([10, 20]));


const anyValues: Array<number | undefined> = [1, 2, undefined, 3];

const numersOnly = anyValues.filter((entry) => !!entry);

const someNumber: unknown = 15;
const someString = someNumber as string;

const anyFunction = (x: string | number): void => {
  if (typeof x === 'string') {
    console.log(x.split(''));
  }
}

type TObjectA = {
  a: number;
}
type TObjectB = {
  b: number;
}

const someSuperFunction = (x: TObjectA | TObjectB): void => {
  const temp = { ...x } as TObjectA;
  if (!!temp.a) {
    console.log(temp.a);
  }
}


enum EDays {
  PONDELI,
  UTERY,
  STREDA,
  CTVRTEK,
  PATEK,
  SOBOTA,
  NEDELE,
}

const today = EDays.PATEK;

const isWeekend = (day: EDays): boolean => {
  return day === EDays.SOBOTA || day === EDays.NEDELE;
}

console.log(isWeekend(today));

type TPerson = {
  name: string;
  age: number;
}
type TDog = {
  color: string;
  age: number;
}

const person: TPerson = {
  name: 'John Vick',
  age: 45,
};

const dog: TDog = {
  color: 'Brown',
  age: 3,
};

const addId = <T>(o: T): T & { id: number } => {
  const id = Math.floor(Math.random() * 1000);
  return { ...o, id };
};

const personWithId = addId<TPerson>(person);
console.log(personWithId.name);
const dogWithId = addId<TDog>(dog);
console.log(dogWithId.color);
