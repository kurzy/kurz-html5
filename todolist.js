const wrapper = document.querySelector('#todolist');

console.log(wrapper);

wrapper.textContent = 'Hello word';

const checkbox = document.createElement('input');
checkbox.type = 'checkbox';
checkbox.value = true;
checkbox.setAttribute('role', 'switch');

wrapper.appendChild(checkbox);

const vek = 30;
// if (vek < 18) {
//   console.log('Žádný pivo nebude!');
// } else if (vek > 65) {
//   console.log('Babička vám povolila jen jedno pivo!');
// } else {
//   console.log('Kolik piv to dnes bude?');
// }

// let message;
// if (vek < 18) {
//   message = 'Žádný pivo nebude!';
// } else {
//   message = 'Kolik piv to dnes bude?';
// }

// let message = 'Kolik piv to dnes bude?';
// if (vek < 18) {
//   message = 'Žádný pivo nebude!';
// }

// const message = vek < 18 ? 'Žádný pivo nebude!' : 'Kolik piv to dnes bude?';
const message = vek < 18
  ? 'Žádný pivo nebude!'
  : 'Kolik piv to dnes bude?';

console.log(message);

let count = 3;
/*
let isCorrectNumber;
do {
  const answer = prompt('Kolik růží?');
  isCorrectNumber = !!answer;
  if (isCorrectNumber) {
    count = parseInt(answer);
    isCorrectNumber = !Number.isNaN(count)
      && count > 0
      && count.toString().length === answer.length;
  }
} while (!isCorrectNumber);
*/

let substantivum;

switch (count) {
  case 1:
    substantivum = 'ruža';
    break;
  case 2:
  case 3:
  case 4:
    substantivum = 'ruže';
    break;
  default:
    substantivum = 'ruží';
}

console.log(count, substantivum);

for (let i = 1; i <= 10; i++) {
  console.log(i);
}

const activities = [
  'jdi na pivo',
  'jdi na panáka se sousedem',
  null,
  'jdi do kina',
  'jdi do kina s partnerem',
  'jdi na nákup',
  '',
  'koukej na televizi',
  // false,
  'jdi na procházku',
  undefined,
  'jdi si zhuntovat tělo',
];

// const predicate = (item) => {
//   return !!item;
// };
// const predicate = (item) => !!item;
// const filteredActivities = activities.filter(predicate);

const filteredActivities = activities.filter((item) => !!item);
console.log(filteredActivities);

const mappedActivities = filteredActivities.map((item) => {
  const chars = item.split('');
  chars[0] = chars[0].toUpperCase();
  const sentence = chars.join('');
  return `${sentence}!`;
});
console.log(mappedActivities);

const a = 5;
const b = 10;
console.log(`Součet čísel ${a} a ${b} je ${a + b}.`);

const activitiesCount = mappedActivities.length;

const randomIndex = Math.floor(Math.random() * activitiesCount);

console.log(mappedActivities[randomIndex]);

// const charCount = mappedActivities.reduce((totalCount, item) => {
//   return totalCount + item.length;
// }, 0);

const charCount = mappedActivities.reduce(
  (totalCount, item) => totalCount + item.length,
  0,
);

console.log(charCount);

const me = {};
me.name = 'Vilem';
me.age = 50;
me.city = 'Brno';

console.log(me);
console.log(me.toString());

me.toString = function () {
  const data = `name: ${this.name}, age: ${this.age},  city: ${this.city}`;
  return data;
}

console.log(me.toString());

const uncle = {
  name: 'Hugo',
  age: 60,
  city: 'New York',
};

uncle.isMarried = true;

console.log(uncle.name, uncle.isMarried);

const jsonData = JSON.stringify(uncle);
console.log(jsonData);

const uncleZombie = JSON.parse(jsonData);
console.log(uncleZombie);
console.log(uncleZombie === uncle);

const uncleClone = uncle;
console.log(uncleClone === uncle);
console.log(uncleClone, uncle);
uncle.wife = 'Eulálie';
console.log(uncleClone, uncle);

uncle.children = ['Evžen', 'Dežo', 'Emil', 'Helga', 'Helmut'];
console.log(uncleClone, uncle);

const uncleCopy1 = { ...uncle };
uncle.age = 61;
console.log(uncleCopy1, uncle);
uncle.children.push('Miloš');

console.log(uncleCopy1, uncle);

const uncleCopy2 = { ...uncle, isCopy: true };
uncle.age = 62;
console.log(uncleCopy2, uncle);

const uncleCopy3 = { ...uncle, isCopy: true, age: 21 };
uncle.age = 63;
console.log(uncleCopy3, uncle);

const uncleCopy4 = { ...uncle, isCopy: true, children: [...uncle.children] };
uncle.age = 64;
uncle.children.push('Elemír');
console.log(uncleCopy4, uncle);

const uncleCopy5 = structuredClone(uncle);
uncle.age = 65;
uncle.children.push('Gejza');
console.log(uncleCopy5, uncle);

const showUncle = (person) => {
  const { name, wife, age: uncleAge, children } = person;
  const [first, second] = children;
  console.log(name, wife, uncleAge, first, second);
};

showUncle(uncle);

// checkbox.onchange = (event) => {
//   const { target } = event;
//   const { checked } = target;
//   console.log(checked);
// };

// checkbox.onchange = (event) => {
//   const { target: { checked } } = event;
//   console.log(checked);
// };

const listener = (event) => {
  const { target: { checked } } = event;
  console.log(checked);
};

checkbox.addEventListener('change', listener);

checkbox.removeEventListener('change', listener);


const upperActivities = activities.map((item) => item?.toUpperCase());
console.log(upperActivities);

const promise1 = new Promise((resolve, reject) => {
  const number = Math.random();
  if (number < 0.5) {
    reject('Promise1: Je to malý!');
  }

  resolve(100 + number);
});

// promise1
//   .then((data) => console.log(data))
//   .catch((data) => console.log(data));

const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    const number = Math.random();
    if (number < 0.5) {
      reject('Promise2: Je to malý!');
    }

    resolve(number);
  }, 1500);
});

/*
promise2
  .then((data) => console.log(data))
  .catch((data) => console.log(data));
*/

const asyncFce = async () => {
  try {
    const data1 = await promise1;
    console.log('promise 1', data1);
    const data = await promise2;
    console.log('async', data);
  } catch (error) {
    console.log('error', error);
  }
};

asyncFce();

console.log('Jsem za async funkcí');

class Person {
  #name;
  #age;
  constructor(name, age) {
    this.#name = name;
    this.#age = age;
  }

  get name() {
    return this.#name;
  }
  get age() {
    return this.#age;
  }

  set age(value) {
    if (typeof value === 'number' && value > 0) {
      this.#age = value;
    }
  }

  whoAmI() {
    return `My name is ${this.#name} and I'm ${this.#age}.`;
  }

  toString() {
    return this.whoAmI();
  }
}

const meMe = new Person('Vilda', 50);

console.log(meMe.name, meMe.age);

meMe.age = 33;

console.log(meMe.name, meMe.age);

console.log(meMe.whoAmI());
console.log(meMe.toString());

class Employee extends Person {
  #salary;
  constructor(name, age, salary) {
    super(name, age);
    this.#salary = salary;
  }

  toString() {
    const personString = super.toString();
    return `${personString} My salary is € ${this.#salary}.`;
  }
}

const eda = new Employee('Eda', 64, 1000);
console.log(eda.toString());
