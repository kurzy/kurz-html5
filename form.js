const form = document.forms[0];

const dom = {
  inputs: {
    category: form.querySelector('#category'),
    done: form.querySelector('#done'),
    dueDate: form.querySelector('#dueDate'),
    solverEmail: form.querySelector('#email'),
    solverName: form.querySelector('#name'),
    taskname: form.querySelector('#taskname'),
  },
  submitButton: form.querySelector('button[type="submit"]'),
};

const sendData = async (data) => {
  const url = 'https://jsdev.cz/xhr/';
  const search = new URLSearchParams(data);

  try {
    const response = await fetch(`${url}?${search}`);
    if (!response.ok) {
      throw new Error(`status code: ${response.status}`);
    }

    const answer = await response.text();
    return answer;
  } catch (error) {
    console.error(error);
  }
};

dom.submitButton.addEventListener('click', async (event) => {
  event.preventDefault();

  validityArray = Object.entries(dom.inputs).map((entry) => {
    const [key, element] = entry;
    const { checked, name, type, value } = element;
    return {
      checked,
      key,
      name,
      type,
      validity: !!element?.validity.valid,
      value,
    };
  });

  // console.log(validityArray);

  const validateElement = (entry) => entry.validity;
  const validForm = validityArray.every(validateElement);
  // console.log(validForm);

  // console.log('form validity', form.checkValidity());
  if (!validForm) {
    return;
  }

  const data = validityArray.reduce((reducer, entry) => {
    const { value, checked, type, name } = entry;
    if (type === 'checkbox') {
      reducer[name] = checked;
      return reducer;
    }

    reducer[name] = value;
    return reducer;
  }, {});

  console.log(data);
  const serverResponse = await sendData(data);
  console.log(serverResponse);
});
